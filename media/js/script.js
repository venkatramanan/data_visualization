var navcontent;
$(document).ready(function(){
   
    /************************************[Sidebar Search Button]************************************/       
    $('.Search-Button').click(function() {
		$('.SideBar-Form-Group').toggle();
		$(this).toggleClass('Search-Button-Active')
	});

	/************************************[Header User Name]************************************/          
	$('.Admin-User').click(function() { 
		$(this).toggleClass('Admin-User-Active')
		$('.Admin-Submenu-Conatiner').toggle();
	});

	/************************************[Collapse Div]************************************/      
    $('.Button-toggle').click(function() {
		$(this).toggleClass('Button-toggle-Active');
		$('.Wrapper').toggleClass('Content-Wrapper-Collapse');
    });
    
	/************************************[Header Sub Naviagation]************************************/    
	$(".SideBar-DropDown li a").click(function(e) {
		var link = ($(this).attr("href"));
		var activemenu = $(this);
		e.preventDefault();
		var showNavi = $(link);  
		var hidemenu = $('.SideBar-DropDown li.Active');
		var hidenavi = $(".SubNavi.ActiveMenu");

		if ($(link).hasClass('ActiveMenu') == false) {    
             $(activemenu).parent().addClass('Active');
             $(showNavi).addClass('ActiveMenu');             
             $(hidenavi).removeClass('ActiveMenu'); 
             $(hidemenu).removeClass('Active');
		} else {
            
		}
	});
});
