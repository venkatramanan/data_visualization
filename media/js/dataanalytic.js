function loadwidget(requestURL, replacementDiv){
    $.ajax({
        'url': requestURL,
        'cache': false,
        'success': function(html) {
            $("#"+replacementDiv).empty().append(html);
        }
    });
}

$('.Admin-User').click(function() { 
	$(this).toggleClass('Admin-User-Active');
    $('.Admin-Submenu-Conatiner').toggle();
});
