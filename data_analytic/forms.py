from django import forms
import re

from data_analytic.models import ReportInfo, DataSource

class LoginForm(forms.Form):
    username = forms.CharField(label=("Username"), max_length=30)
    password = forms.CharField(label=("Password"), widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):        
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'class': 'form-control'})
        self.fields['password'].widget.attrs.update({'class': 'form-control'})

    def clean_username(self):
        username = self.cleaned_data['username']
        if  re.match(r'^[\W\d]*$', username):
            raise forms.ValidationError("Enter a valid username.")
        return username

    def clean_password(self):
        password = self.cleaned_data['password']
        if  re.match(r'^[\s]*$', password):
            raise forms.ValidationError("Enter a valid password.")
        return password
    

class ReportForm(forms.Form):
    
    reportInfoObj = ReportInfo()
    CHART_TYPE_CHOICES = reportInfoObj.CHART_TYPE_CHOICE
    DATASOURCE_CHOICES = DataSource.objects.values_list('id', 'name')
    FILTER_CHOICES = (('date','Date'), ('month','metrics'))

    name = forms.CharField(label=("Report Name"), max_length=30, required=True)
    description = forms.CharField(label=("Description"), max_length=30, required=False)
    datasource = forms.ChoiceField(label=("DataSource"), choices=DATASOURCE_CHOICES, required=True)
    chart_type = forms.ChoiceField(label=("Chart Type"), choices=CHART_TYPE_CHOICES, required=True)
    height = forms.CharField(label=("Height"), max_length=5, required=True)
    width = forms.CharField(label=("Width"), max_length=5, required=True)
    is_chart = forms.BooleanField(label=("Is Chart"), required=False)
    is_tabular = forms.BooleanField(label=("Is Tabular"), required=False)
    is_filter = forms.BooleanField(label=("Is Filter"), required=False)
    filter_column = forms.ChoiceField(label=("Filter Column"), choices=FILTER_CHOICES, required=False)


    def __init__(self, *args, **kwargs):        
        super(ReportForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'class': 'CommonField TextBox'})
        self.fields['description'].widget.attrs.update({'class': 'CommonField TextBox'})
        self.fields['datasource'].widget.attrs.update({'class': 'CommonField TextBox'})        
        self.fields['chart_type'].widget.attrs.update({'class': 'CommonField'})
        self.fields['height'].widget.attrs.update({'class': 'CommonField TextBox'})
        self.fields['width'].widget.attrs.update({'class': 'CommonField TextBox'})
        self.fields['is_chart'].widget.attrs.update({'class': 'CommonField TextBox'})
        self.fields['is_tabular'].widget.attrs.update({'class': 'CommonField TextBox'})        
        self.fields['is_filter'].widget.attrs.update({'class': 'CommonField TextBox'})        
        self.fields['filter_column'].widget.attrs.update({'class': 'CommonField'})        

        if not self.fields['chart_type'].choices[0][0] == '':
            self.fields['chart_type'].choices.insert(0, ('', '--------- Select Chart Type ---------' ) )
        
        if not self.fields['datasource'].choices[0][0] == '':
            self.fields['datasource'].choices.insert(0, ('', '--------- Select Datasource ---------' ) )
