from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'data_analytic.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^demo/', include('data_analytic.demo_urls')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += patterns('data_analytic.views',
    # Report Creation:
    url(r'^$','dashboard'),
    url(r'^chart/(\d+)/$','show_chart'),
    url(r'^reports/$','reports'),
    url(r'^dashboard/$','dashboard'),
    
    url(r'^getDimensionsAndMetrics/(?P<datasourceId>.+)$', 'getDimensionsAndMetrics'),
    #url(r'^viewreport/(?P<reportId>.+)/(?P<reportType>.+)/$', 'viewReport'),
    url(r'^dashboard/$', 'dashboard'),
    url(r'^exploredata/$', 'explore_data'),
    url(r'^exploredata/(\d+)/$', 'explore_data'),
    url(r'^exploredata/(?P<source_id>\d)/(?P<fdate>\d{4}-\d{2}-\d{2})/(?P<tdate>\d{4}-\d{2}-\d{2})$','explore_data'),
    url(r'^heatmap/$', 'heatmap'),
    url(r'^getHeatMap/(?P<geoId>.+)/(?P<startDate>\d{4}-\d{2}-\d{2})/(?P<endDate>\d{4}-\d{2}-\d{2})/$', 'getHeatMap'),

    # login url details
    url(r'^accounts/login/$', 'login'),
    url(r'^accounts/logout/$', 'logout'),
    url(r'^accounts/settings/$', 'usersettings'),
    url(r'^accounts/changepassword/$', 'changepassword'),

    url(r'^accounts/userreportslist/$', 'userreports'),
    
    url(r'^accounts/reports/(?P<action>.+)/(?P<reportId>.+)/$', 'userreportdetails'),
    url(r'^accounts/reports/(?P<action>.+)/$', 'userreportdetails'),

    # Add to Dashboard url
    url(r'^addDashboard/(\d+)/$', 'addDashboard'),
    url(r'^removeDashboard/(\d+)/$', 'removeDashboard'),
)