from django.contrib import admin
from data_analytic.models import DataSource, DataFields, ReportInfo, Dashboard

class DataSourceAdmin(admin.ModelAdmin):
    list_display = ('name', 'path', 'description')
    list_display_links = ('name',)
    search_fields = ['name','path']
    
class DataFieldsAdmin(admin.ModelAdmin):
    list_display = ('name', 'field_type', 'displayname','datasource')
    list_display_links = ('name',)
    list_filter = ('datasource', 'field_type')
    search_fields = ['name', 'field_type', 'displayname']
    
class ReportInfoAdmin(admin.ModelAdmin):
    list_display = ('name', 'chart_type','datasource', 'height','width')
    list_display_links = ('name',)
    list_filter = ('datasource', 'chart_type')
    search_fields = ['name', 'chart_type']
    
class DashboardAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_display_links = ('name',)
    #list_filter = ('datasource', 'chart_type')
    search_fields = ['name', 'report_info__name']

admin.site.register(DataSource, DataSourceAdmin)
admin.site.register(DataFields, DataFieldsAdmin)
admin.site.register(ReportInfo, ReportInfoAdmin)
admin.site.register(Dashboard, DashboardAdmin)