import sys
import json
import numpy

import datetime
from datetime import date
from time import strptime, strftime
from django.conf import settings

SHAPES = ['circle', 'cross', 'triangle-up','diamond', 'square','triangle-down',]
#EXTRA_SERIES = { 'scatterChart':{'extra%d':{"tooltip": {"y_start": "", "y_end": " balls"}}}
#}


def get_filter_data(json_data, filters):
    chart_data = []
    if filters['filter_column'] == 'Date':
        for d in json_data:
            if (d['date'] >= filters['startDate'] and d['date'] <= filters['endDate']):
                chart_data.append(d)
    if filters['filter_column'] == 'Month':
        for d in json_data:
            if (d['month'] == filters['month']):
                chart_data.append(d)
    return json.dumps(chart_data)


def get_chart_data(json_path, x_axis, y_axis, report_info, report_data_fields, filters=None):
    #json_data = open(json_path).read()
    #data1 = json.loads(json_data)
    data1 = _getRecords(report_info, report_data_fields)

    if filters:
        data1 = json.loads(get_filter_data(data1, filters))
        if not data1:
            return None

    chart_data = {}
    for d in data1:
        #x-axis data
        if(d[x_axis] and (not chart_data.get(d[x_axis], None))):
            chart_data.update({d[x_axis]: []})
        #y-axis data
        y_data = []
        for y in y_axis:
            if(d[y] and (not chart_data.get(d[y], None))):
                y_data.append(d[y])
        chart_data[d[x_axis]].append(y_data)
    # extracting the x and y values from the dict
    fmt = '%Y-%m-%d'
    x_data_raw = chart_data.keys()
    x_data = []
    if x_axis.find('date') > -1:
        x_data = [datetime.datetime.strptime(k, fmt) for k in x_data_raw]
        x_data.sort()
        x_data = [x.strftime(fmt) for x in x_data]
    elif type(x_data_raw[0]) == str and x_data_raw[0].isdigit():
        x_data = [int(k) if k.isdigit() else k for k in x_data_raw]
        x_data.sort()
    else:
        x_data = x_data_raw
        x_data.sort()
    data = {'x': x_data}
    series_data = []
    series_list = []

    for y_data in y_axis:
        if (report_data_fields.filter(name=y_data)[0].field_type == 'DIM'):
            for item in data1:
                series_data.append(item[y_data])
            series_list = list(set(series_data))
    extra_serie = {"tooltip": {"y_start": " ", "y_end": " "}, }
    j = 0

    if(len(series_list) > 0):
        for i in series_list:
            y_value = []
            for x_value in x_data:
                if(type(x_value) is int):
                    y0 = chart_data.get('%d' % (x_value), [])
                    y2 = [int(k[1]) for k in y0 if k[0] == i]
                else:
                    y0 = chart_data.get((x_value), [])
                    y2 = [int(k[1]) for k in y0 if k[0] == i]
                y_value.append(numpy.sum(y2))
            y_index = 'y%d' % (j + 1)
            y_name = i
            y_name_index = 'name%d' % (j + 1)
            y_extra = 'extra%d' % (j + 1)
            j = j + 1
            data.update({y_name_index: y_name,y_index: y_value,y_extra: extra_serie})
    else:
        for i in range(len(y_axis)):
            y_value = []
            for x_value in x_data:
                if(type(x_value) is int):
                    y1=[int(v[i]) for v in chart_data.get(u'%d'%(x_value),[]) if v and len(v)>i]
                else:
                    y1=[int(v[i]) for v in chart_data.get(x_value,[]) if v and len(v)>i]
                y_value.append(numpy.sum(y1))
            y_index = 'y%d' % (i + 1)
            y_name = y_axis[i]
            y_name_index = 'name%d' % (i + 1)
            y_extra = 'extra%d' % (i + 1)
            data.update({y_name_index: y_name,y_index: y_value,y_extra: extra_serie})
        if(report_info.chart_type =='scatterChart'):
            kwarg = 'kwargs%d' % (i + 1)
            data.update({kwarg: {'shape': SHAPES[(i + 1) % 6]}})
    return data


def getWeekDetails():
    _weekNo = date.today().isocalendar()[1] - 1
    _Year = date.today().year
    _weekStart = 2
    rslt = []
    janOne = strptime('%s-01-01' % _Year, '%Y-%m-%d')
    dayOfFirstWeek = ((7-int((strftime("%u",janOne)))+ int(_weekStart)) % 7)
    if dayOfFirstWeek == 0:
        dayOfFirstWeek = 7
    dateOfFirstWeek = strptime('%s-01-%s' % (_Year, dayOfFirstWeek), '%Y-%m-%d')
    dayOne = datetime.datetime( dateOfFirstWeek.tm_year, dateOfFirstWeek.tm_mon, dateOfFirstWeek.tm_mday )
    daysToGo = 7*(int(_weekNo)-1)
    lastDay = daysToGo+6
    dayX = dayOne + datetime.timedelta(days = daysToGo)
    dayY = dayOne + datetime.timedelta(days = lastDay)
    resultDateX = strptime('%s-%s-%s' % (dayX.year, dayX.month, dayX.day), '%Y-%m-%d')
    resultDateY = strptime('%s-%s-%s' % (dayY.year, dayY.month, dayY.day), '%Y-%m-%d')
    rslt.append(resultDateX)
    rslt.append(resultDateY)
    return rslt

from useraccess.models import GroupReport
def dataanalytics_perimission(view_func):
    def decorator(request, *args, **kwargs):
        if request.user.id:
            groupIds = [group.pk for group in request.user.groups.all()]
            group_reports = GroupReport.objects.filter(group_id__in = groupIds)
            reports_info = set([group.report_info.all() for group in group_reports])
            if reports_info:
                return reports_info #view_func(request, *args, **kwargs)
            else:
                messages.success(request,
                ("Sorry,You don't have permissions for access this page"),
                fail_silently=True)
                return redirect('/')
        else:
            return redirect('/accounts/login/')
    return view_func


def _getRecords(report_info, report_data_fields):    
    if report_info.datasource.is_database:
        return _fetchRecordFromDB(report_info, report_data_fields)
    else:
        return _fetchRecordFromFile(report_info)


def _fetchRecordFromDB(report_info, report_data_fields):
    import collections
    records = []
    if report_info.datasource.db_provider == "sqlite":
        import sqlite3
        conn = sqlite3.connect('%s/%s' % (settings.BASE_ROOT, 
                        report_info.datasource.database_name))
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        objects_list = []
        for row in cur.execute("SELECT * FROM %s" % (report_info.datasource.table_name)):
            d = collections.OrderedDict()
            for field in report_data_fields:
                d[str(field.name)] = str(row[str(field.name)])
            objects_list.append(d)
        records = json.loads(json.dumps(objects_list))
        conn.commit()
        conn.close()
        return records
    elif report_info.datasource.db_provider == "mysql":
        import MySQLdb
        conn = MySQLdb.connect(host=report_info.datasource.server_name,
                            user=report_info.datasource.user_name,
                            passwd=report_info.datasource.password,
                            db=report_info.datasource.database_name)
        cur = conn.cursor(MySQLdb.cursors.DictCursor)
        cur.execute("SELECT * FROM %s" % (report_info.datasource.table_name))
        objects_list = []
        for row in cur.fetchall():
            d = collections.OrderedDict()
            for field in report_data_fields:
                d[str(field.name)] = str(row[str(field.name)])
            objects_list.append(d)
        records = json.loads(json.dumps(objects_list))
        cur.close()
        conn.close ()
        return reports


def _fetchRecordFromFile(report_info):
    file_path = '%s%s' % (settings.BASE_ROOT, report_info.datasource.path)
    import csv
    if file_path.endswith(".tsv"):
        records = []
        data = csv.DictReader(open(file_path), dialect="excel-tab")
        for row in data:
            records.append(row)
        return records
    elif file_path.endswith(".csv"):
        records = []
        data = csv.DictReader(open(file_path), delimiter=',')
        for row in data:
            records.append(row)
        return records
    elif file_path.endswith(".json"):
        json_data = open(file_path).read()
        return json.loads(json_data)
