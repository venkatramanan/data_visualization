from django.shortcuts import render, render_to_response, redirect
from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.conf import settings
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
import json
import datetime

from data_analytic.chart_meta import ChartMeta
from data_analytic.utility import *
from data_analytic.models import *
from data_analytic.forms import LoginForm, ReportForm
from useraccess.models import *


def _get_report_data_(report_id, filters=None):
    report_info = ReportInfo.objects.get(pk=report_id)
    json_path = '%s%s' % (settings.BASE_ROOT, report_info.datasource.path)
    data_fields = DataFields.objects.filter(datasource=report_info.datasource.pk)

    x_axis = report_info.x_axis.all()[0].name
    y_axis = [y.name for y in report_info.y_axis.all()]
    chart_data = get_chart_data(json_path, x_axis, y_axis, report_info, data_fields, filters)
    if chart_data is None:
        return None
    chart_meta = ChartMeta().get_chart_meta(report_info.chart_type)
    data_keys = chart_data.keys()
    for data_key in data_keys:
        chart_meta['chartdata'].update({data_key:chart_data.get(data_key, [])})
    chart_meta['chartcontainer'] = '%s_%s' % (chart_meta['chartcontainer'], report_info.name.replace(' ', '').lower())
    chart_meta.update({'report_name': report_info.name, 'width':report_info.width,
                       'height': report_info.height, 'report_id': report_info.pk})
    print(chart_meta)
    return chart_meta


@login_required
def show_chart(request, report_id, report_all=None):
    report_all = get_user_reports(request)
    filter_params = getFilterParams(report_id)
    error_msg = ''
    if request.method == "POST":
        return_value = _getFilteredData(request, filter_params)
        chart_meta = return_value['chart_meta']
        error_msg = return_value['errorMsg']
        selected_month = request.POST.get('ddlMonth')
    else:
        chart_meta = _get_report_data_(report_id)
        selected_month = '01'
    return  render_to_response('home.html', {'chart': chart_meta,
                                            'reports': report_all,
                                            'page_title': 'Reports',
                                            'report_id': report_id,
                                            'filter_params': filter_params,
                                            'selected_month': selected_month,
                                            'error_msg': error_msg},
                            context_instance=RequestContext(request))


@login_required
def reports(request):
    report_all = get_user_reports(request)
    filter_params = getFilterParams(report_all[0].pk)
    error_msg = ''
    if request.method == "POST":
        return_value = _getFilteredData(request, filter_params)
        chart_meta = return_value['chart_meta']
        error_msg = return_value['errorMsg']
        selected_month = request.POST.get('ddlMonth')
    else:
        chart_meta = _get_report_data_(report_all[0].pk) if report_all else []
        selected_month = '01'
    return  render_to_response('home.html', {'chart': chart_meta,
                                            'reports': report_all,
                                            'page_title': 'Reports',
                                            'report_id': report_all[0].pk,
                                            'filter_params': filter_params,
                                            'selected_month': selected_month,
                                            'error_msg': error_msg},
                                context_instance=RequestContext(request))


def _getFilteredData(request, filter_params):
    error_msg = ''
    if filter_params['filter_column'] == 'Date':
        filters = {'startDate': request.POST.get('txtFromDate'),
                        'endDate': request.POST.get('txtToDate')}
    elif filter_params['filter_column'] == 'Month':
        filters = {'month': request.POST.get('ddlMonth'),
                    'year': request.POST.get('ddlYear')}
    filters.update({'filter_column': filter_params['filter_column']})
    chart_meta = _get_report_data_(report_all[0].pk, filters) if report_all else []
    if chart_meta is None:
        error_msg = "No Data Found"
    params = {'chart_meta': chart_meta, 'errorMsg': error_msg}
    return params


def getFilterParams(reportId):
    report_info = ReportInfo.objects.get(pk=reportId)
    years = [year for year in range(2013, (datetime.datetime.now().year + 1))]
    params = {'is_filter': report_info.is_filter,
                    'filter_column': report_info.filter_column,
                    'years': years}
    return params


def get_user_reports(request):
    report_data = []
    if request.user.id:
        groupIds = [group.pk for group in request.user.groups.all()]
        group_reports = GroupReport.objects.filter(group_id__in = groupIds)
        reports_info = [group.report_info.all() for group in group_reports]
        for reports in reports_info:
            for report in reports:
                report_data.append(report)
    return report_data


@login_required
def dashboard(request):
    data = []
    userId = request.session.get('userId', '')
    userDashboards = UserDashboard.objects.filter(user_id=userId)
    for userDashboard in userDashboards:
        for report in userDashboard.dashboard.report_info.all():
            chart_meta = _get_report_data_(report.pk)
            data.append(chart_meta)
    return  render_to_response('dashboard.html', {'charts':data,
                                            'page_title':'Dashboard'},
                                context_instance=RequestContext(request))


    data = None
    json_path = '%s%s' % (settings.BASE_ROOT,settings.MEDIA_WIDGET)
    json_data = open(json_path).read()
    data = json.loads(json_data)
    datasources = list(data.keys())
    datasources.sort()
    report_Info = ReportInfo()
    chart_type = report_Info.CHART_TYPE_CHOICE
    charttype = []
    for chart in chart_type:
        charttype.append(chart[1])
    return render(request, 'reportcreation.html', {'data': data,
                                                    'datasources': datasources,
                                                    'charttype': charttype})


def getDimensionsAndMetrics(request, datasourceId):
    dimensions = DataFields.objects.values_list('id', 'name').filter(datasource=datasourceId).filter(field_type="DIM")
    metrics = DataFields.objects.values_list('id', 'name').filter(datasource=datasourceId).filter(field_type="MET")
    data = json.dumps({'dimensions': [{'id':data[0], 'name':data[1]} for data in dimensions],
                        'metrics': [{'id':data[0], 'name':data[1]} for data in metrics] })
    return HttpResponse(data, content_type="application/json")


    try:
        datasource = widget_datasource()
        datasource.datasource_name = request.POST.get("datasourcename")
        datasource.reportname = request.POST.get("reportname")
        datasource.charttype = request.POST.get("charttype")
        datasource.save()

        widget_datasource_id = widget_datasource.objects.latest('id').pk

        dimensionList = request.POST.getlist("dimension[]")
        _saveDataSourceChilds(widget_datasource_dimensions,
                                widget_datasource_id, dimensionList)

        metricsList = request.POST.getlist("metric[]")
        _saveDataSourceChilds(widget_datasource_metrics, widget_datasource_id,
                                                                metricsList)

        chartYAxisList = request.POST.getlist("chartyaxis[]")
        _saveDataSourceChilds(widget_chart_yaxis, widget_datasource_id,
                                                                chartYAxisList)

        chartXAxisList = request.POST.getlist("chartxaxis[]")
        _saveDataSourceChilds(widget_chart_xaxis, widget_datasource_id,
                                                                chartXAxisList)

    except Exception as e:
        print(e)
        return False
    return True


@login_required
def explore_data(request, source_id=None, fdate=None, tdate=None):
    datasource = DataSource.objects.all()
    source_id = source_id if source_id != None else datasource[0].pk
    report_info = DataSource.objects.get(pk=source_id)
    data_fields = DataFields.objects.filter(datasource=source_id)
    json_path = '%s%s' % (settings.BASE_ROOT,report_info.path)
    json_data = open(json_path).read()
    data = json.loads(json_data)
    if(fdate is not None and tdate is not None):
        data = [i for i in data if i['date'] >= fdate and i['date'] <= tdate]
        result = json.dumps(data)
        return HttpResponse(result, content_type="application/json")
    fields = []
    coldefs = []
    dates = []
    if data[0].has_key('date'):
        for d in data:
            dates.append(d['date'])
    #if(str(source_id) == "1" or str(source_id) == "4" or str(source_id) == "5"):
        #for d in data:
            #dates.append(d['date'])
    #elif(str(source_id) != "9" and str(source_id) != "10"):
        #for d in data:
            #dates.append(d['year_month'])
    if(len(dates) > 0):
        dateset = set(dates)
        datelist = list(dateset)
        datelist.sort()
        startdt = datelist[0]
        enddt = datelist[-1]
    else:
        startdt = None
        enddt = None
    #data = data.sort(key=lambda x: datetime.datetime.strptime(x['date'], '%Y-%m-%d'))
    for datafield in data_fields:
        fields.append({"field": datafield.name, "type": datafield.field_type,
                        "visible": "true", "groupable": "true",
                        "displayName": datafield.description})
        coldefs.append({"field": datafield.name, "visible": "true",
                        "groupable": "true", "displayName": datafield.displayname})
    return render_to_response('tabularhome.html', {'datasource': datasource,
                                                'page_title': 'ExploreData',
                                                'report_info': report_info,
                                                'path': report_info.path,
                                                'fields': fields,
                                                'sourceid': source_id,
                                                'data': data,
                                                'fromdate': startdt,
                                                'todate': enddt,
                                            'colmndef': json.dumps(coldefs),
                                            'username': request.session.get('username', '')},
                                context_instance=RequestContext(request))


@login_required
def heatmap(request):
    json_path = '%s%s' % (settings.BASE_DIR, '/media/heatmap.json')
    json_data = open(json_path).read()
    data = json.loads(json_data)
    dates = []
    for date in data:
        dates.append(date['date'])

    distinctDates = set(dates)
    convertToList = list(distinctDates)
    convertToList.sort()
    lastWeekDates = convertToList[-7:]
    weekStartDate = lastWeekDates[0]
    weekEndDate = lastWeekDates[6]
    return render_to_response('heatmap.html', {'page_title' : 'Heatmap',
                                                'startDate': weekStartDate,
                                                'endDate': weekEndDate, 'geoId': 1},
                                context_instance=RequestContext(request))


def getHeatMap(request, geoId, startDate, endDate):
    json_path = '%s%s' % (settings.BASE_DIR, '/media/heatmap.json')
    json_data = open(json_path).read()
    data = json.loads(json_data)
    chart_data = []
    for d in data:
        if (d['date'] >= startDate and d['date'] <= endDate):
            chart_data.append(d)
    result = json.dumps(chart_data)
    return HttpResponse(result, content_type="application/json")


@csrf_exempt
def login(request):
    form = LoginForm()
    errors = ''
    from django.contrib.auth import authenticate
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            uname = form.data['username']
            passwd = form.data['password']
            user = authenticate(username=uname, password=passwd)
            if user is not None:
                request.session['userId'] = user.id
                request.session['username'] = form.cleaned_data['username']
                auth_login(request, user)
                return redirect(settings.LOGIN_REDIRECT_URL)
            else:
                errors = "The username or password you entered is incorrect."
        else:
            errors = form.errors
    return render_to_response('login.html', {'form': form,
                                        'errors': errors,
                                        'page_title': 'Login'},
                                       context_instance=RequestContext(request))


def logout(request):
    auth_logout(request)
    return redirect(settings.LOGIN_URL)


def addDashboard(request, reportId):
    userid = request.user.id
    print userid
    data = ''
    if userid:
        userDashboardId = UserDashboard.objects.get(user_id=userid).dashboard_id
        dashboardobj = Dashboard.objects.get(pk=userDashboardId)
        reportinfo = dashboardobj.report_info.filter(pk=reportId)
        if reportinfo:
            data = "Reports already added to dashboard"
        else:
            reportsObj = ReportInfo.objects.get(pk=reportId)
            dashboardobj.report_info.add(reportsObj)
            data = "Reports successfully added to dashboard"
    return HttpResponse(data, content_type="html/text")


@login_required
def userreports(request):
    print request.user
    return _bindReportList(request)


def changepassword(request):
    message = ''
    if request.user.id:
        if request.method == "POST":
            password = request.POST.get('txtNewPswd')
            userObj = User.objects.get(pk=request.user.id)
            userObj.set_password(password)
            userObj.save()
            message = "Your Password has been changed successfully."
    return render_to_response('changepassword.html', {'message': message,
                                    'page_title': 'Change Password',
                                'username': request.session.get('username', '')
                                }, context_instance=RequestContext(request))


def removeDashboard(request, reportId):
    userid = request.user.id
    data = ''
    if userid:
        userDashboardId = UserDashboard.objects.get(user_id=userid).dashboard_id
        dashboardobj = Dashboard.objects.get(pk=userDashboardId)
        reportsObj = ReportInfo.objects.get(pk=reportId)
        dashboardobj.report_info.remove(reportsObj)
        data = "Reports successfully removed from dashboard"
    return HttpResponse(data, content_type="html/text")


@login_required
def usersettings(request):
    return render_to_response('settings.html', {'page_title': 'Settings',
                          'username': request.session.get('username', '')},
                                context_instance=RequestContext(request))


@login_required
def userreportdetails(request, action, reportId=None):
    reportForm = ReportForm()
    if action == "add":
        if request.method == "POST":
            if _saveReport(request, reportId):
                msg = "%s Report Successfully Created" % (request.POST.get('name'))
                return _bindReportList(request, msg)
            else:
                return render_to_response('reportdetails.html', {'form': reportForm,
                                        'page_title': 'Report Creation', 'datasources': list(DataSource.objects.values('id', 'name'))},
                                       context_instance=RequestContext(request))
        else:
            return render_to_response('reportdetails.html', {'form': reportForm,
                                        'page_title': 'Report Creation', 'datasources': list(DataSource.objects.values('id', 'name'))},
                                       context_instance=RequestContext(request))
    elif action == "edit":
        if request.method == "POST":
            if _saveReport(request, reportId):
                msg = "%s Report Successfully Updated" % (request.POST.get('name'))
                return _bindReportList(request, msg)
        else:
            return _getReportsById(request, reportId)


def _saveReport(request, reportId):
    try:
        form = ReportForm(request.POST)
        if form.is_valid():
            reportInfo = ReportInfo()
            reportInfo.datasource = DataSource.objects.get(pk=str(request.POST.get('datasource')))
            reportInfo.name = form.data['name']
            reportInfo.chart_type = form.data['chart_type']
            reportInfo.description = form.data['description']
            reportInfo.height = form.data['height']
            reportInfo.width = form.data['width']
            if reportId is not None:
                reportInfo.pk = reportId
                reportObj = ReportInfo.objects.get(pk=reportId)
                for XAxis in reportObj.x_axis.all():
                    datafieldObj = DataFields.objects.get(pk=XAxis.id)
                    reportObj.x_axis.remove(datafieldObj)
                for YAxis in reportObj.y_axis.all():
                    datafieldObj = DataFields.objects.get(pk=YAxis.id)
                    reportObj.y_axis.remove(datafieldObj)
            reportInfo.save()
            for XAxis in request.POST.getlist('ddlXAxis'):
                reportInfo.x_axis.add(XAxis)
            for YAxis in request.POST.getlist('ddlYAxis'):
                reportInfo.y_axis.add(YAxis)
            _addReportByGroupId(request, reportInfo)
        else:
            return False
    except Exception as e:
        print e
        return False
    return True


def _bindReportList(request, msg=None):
    reports = get_user_reports(request)
    return render_to_response('reportlist.html', {'reports': reports,
                                                  'page_title': 'User Reports',
                                                  'msg': msg},
                               context_instance=RequestContext(request))


def _getReportsById(request, reportId):
    reportObj = ReportInfo.objects.get(pk=reportId)
    form = ReportForm(initial=reportObj.__dict__)
    data = json.dumps({'dimensions': [{'id': x_axis.id, 'name': x_axis.name } for x_axis in reportObj.x_axis.all()],
                        'metrics': [{'id': y_axis.id, 'name': y_axis.name} for y_axis in reportObj.y_axis.all()]})
    return render_to_response('reportdetails.html', {'form': form,
                                            'page_title': 'Report Updation',
                                            'datasources': DataSource.objects.values('id', 'name'),
                                            'datasourceId': reportObj.datasource_id,
                                            'data': data},
                               context_instance=RequestContext(request))


def _addReportByUserId(request, reportInfo):
    for group in request.user.groups.all():
        reportGroup = GroupReport.objects.filter(group_id = group.id)
        for reportGrp in reportGroup:
            if not reportGrp.report_info.filter(pk=reportInfo.pk):
                reportGrp.report_info.add(reportInfo)
