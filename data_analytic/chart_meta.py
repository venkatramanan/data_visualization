class ChartMeta:

    def __init__(self):
        #chart color value
        self.color_list = ['#5d8aa8', '#e32636', '#efdecd', '#ffbf00', '#ff033e',
                  '#a4c639', '#b2beb5', '#8db600', '#7fffd4', '#ff007f',
                  '#ff55a3', '#5f9ea0']
        #pie chart metadata
        self.piechart = {
            'charttype': 'pieChart',
            'chartdata': {},
            'chartcontainer': 'piechart_container',
            'extra': {
                'x_is_date': False,
                'x_axis_format': '',
                'tag_script_js': True,
                'jquery_on_ready': True,
            }
        }

        #lien chart
        #self.tooltip_date = "%d %b %Y %H:%M:%S %p"
        self.extra_serie1 = {
            "tooltip": {"y_start": "", "y_end": " cal"},
            "date_format": "%d %b %Y %H:%M:%S %p",
            'color': '#a4c639'
            }
        self.extra_serie2 = {
            "tooltip": {"y_start": "", "y_end": " cal"},
            "date_format": "%d %b %Y %H:%M:%S %p",
            'color': '#FF8aF8'
            }

        self.linechart_dt =  {
            'charttype': 'lineChart',
            'chartdata': {},
            'chartcontainer': 'linechart_container',
            'extra': {
                'x_is_date': True,
                'x_axis_format': '%d %b %Y',
                'tag_script_js': True,
                'jquery_on_ready': False,
            }
        }

        self.linechart = {
            'charttype': 'lineChart',
            'chartdata': {},
            'chartcontainer': 'linechart_container',
            'extra': {
                'x_is_date': False,
                'x_axis_format': '',
                'tag_script_js': True,
                'jquery_on_ready': False,
            }
        }

        #self.tooltip_date = "%d %b %Y %H:%M:%S %p"
        extra_serie = {"tooltip": {"y_start": "There are ", "y_end": " calls"},
                       "date_format": "%d %b %Y %H:%M:%S %p"}
        self.linewithfocuschart = {
            'charttype': 'lineWithFocusChart',
            'chartdata': {},
            'chartcontainer': 'linewithfocuschart_container',
            'extra': {
                'x_is_date': True,
                'x_axis_format': '%d %b %Y %H',
                'tag_script_js': True,
                'jquery_on_ready': True,
            }

        }

        self.multibarchart = {
            'charttype': 'multiBarChart',
            'chartdata': {},
            'chartcontainer': 'multibarchart_container',
            'extra': {
                'x_is_date': False,
                'x_axis_format': '',
                'tag_script_js': True,
                'jquery_on_ready': True,
            }
        }

        self.stackedAreaChart =  {
            'charttype': 'stackedAreaChart',
            'chartdata': {},
            'chartcontainer': 'stackedareachart_container',
            'extra': {
                'x_is_date': False,
                'x_axis_format': '',
                'tag_script_js': True,
                'jquery_on_ready': True,
            },
        }
        self.multiBarHorizontalChart = {
            'charttype': 'multiBarHorizontalChart',
            'chartdata': {},
            'chartcontainer': 'multibarhorizontalchart_container',
            'extra': {
                'x_is_date': False,
                'x_axis_format': '',
                'tag_script_js': True,
                'jquery_on_ready': True,
            },
        }

        #TODO: need to update bar chart
        self.linePlusBarChart =  {
            'charttype': 'linePlusBarChart',
            'chartdata':{},
            'chartcontainer': 'lineplusbarchart_container',
            'extra': {
                'x_is_date': True,
                'x_axis_format': '%d %b %Y %H',
                'tag_script_js': True,
                'jquery_on_ready': True,
            },
        }

        self.cumulativeLineChart = {
            'charttype': 'cumulativeLineChart',
            'chartdata':{},
            'chartcontainer': 'cumulativelinechart_container',
            'extra': {
                'x_is_date': True,
                'x_axis_format': '%d %b %Y %H',
                'tag_script_js': True,
                'jquery_on_ready': True,
            },
        }

        self.discreteBarChart = {
            'charttype': 'discreteBarChart',
            'chartdata':{},
            'chartcontainer': 'discretebarchart_container',
            'extra': {
                'x_is_date': True,
                'x_axis_format': '%d %b %Y %H',
                'tag_script_js': True,
                'jquery_on_ready': True,
            },
        }

        self.discreteBarChart_dt = {
            'charttype': 'discreteBarChart',
            'chartdata':{},
            'chartcontainer': 'discretebarchart_container',
            'extra': {
                'x_is_date': True,
                'x_axis_format': '%d %b %Y %H',
                'tag_script_js': True,
                'jquery_on_ready': True,
            },
        }

        #TODO share need to defined
        self.shapes = ['circle', 'cross', 'triangle-up', 'triangle-down', 'diamond', 'square'],
        self.scatterChart = {
            'charttype': 'scatterChart',
            'chartdata':{},
            'chartcontainer': 'scatterchart_container',
            'extra': {
                'x_is_date': True,
                'x_axis_format': '%d %b %Y %H',
                'tag_script_js': True,
                'jquery_on_ready': True,
            },
        }

        self.linePlusBarWithFocusChart = {
            'charttype': 'linePlusBarWithFocusChart',
            'chartdata':{},
            'chartcontainer': 'lineplusbarwithfocuschart_container',
            'extra': {
                'x_is_date': True,
                'x_axis_format': '%d %b %Y %H',
                'tag_script_js': True,
                'jquery_on_ready': True,
            },
        }

        self.chart_meta = {'pieChart':self.piechart,
                  'lineChart_Date':self.linechart_dt,
                  'lineChart':self.linechart,
                  'lineWithFocusChart': self.linewithfocuschart,
                  'multiBarChart':self.multibarchart,
                  'stackedAreaChart':self.stackedAreaChart,
                  'multiBarHorizontalChart':self.multiBarHorizontalChart,
                  'linePlusBarChart':self.linePlusBarChart,
                  'cumulativeLineChart':self.cumulativeLineChart,
                  'discreteBarChart':self.discreteBarChart,
                  'discreteBarChart_Date':self.discreteBarChart_dt,
                  'scatterChart':self.scatterChart,
                  'linePlusBarWithFocusChart':self.linePlusBarWithFocusChart
                }

    def get_chart_meta(self, chart_type):
        return self.chart_meta.get(chart_type, '')
