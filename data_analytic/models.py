# -*- coding: utf-8 -*-
from django.db import models

from django.contrib.auth.models import User

class DataSource(models.Model):
    DB_PROVIDER_CHOICE = (('sqlite', 'Sqlite'), ('mysql', 'MySql'))
    
    name = models.CharField(max_length=45)
    path = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    is_database = models.BooleanField(default=False)    
    server_name = models.CharField(max_length=50, blank=True, null=True)
    user_name = models.CharField(max_length=50, blank=True, null=True)
    password = models.CharField(max_length=50, blank=True, null=True)
    database_name = models.CharField(max_length=50, blank=True, null=True)
    table_name = models.CharField(max_length=50, blank=True, null=True)
    db_provider = models.CharField(max_length=15, choices=DB_PROVIDER_CHOICE)

    class Meta:
        db_table = "datasource"
        verbose_name = "datasource"

    def __unicode__(self):
        return self.name


class DataFields(models.Model):
    FIELD_TYPE_CHOICE = (('DIM','Diminsion'),
                         ('MET','Metrics'))
    datasource = models.ForeignKey(DataSource)
    name = models.CharField(max_length=80)
    description = models.TextField(blank=True, null=True)
    displayname = models.CharField(max_length=80, null=True)
    field_type = models.CharField(max_length=3,choices=FIELD_TYPE_CHOICE)

    class Meta:
        db_table = "data_fields"
        verbose_name = "data_fields"

    def __unicode__(self):
        return u'%s:%s' % (self.datasource.name, self.name)


class ReportInfo(models.Model):
    CHART_TYPE_CHOICE = (('pieChart','PieChart'),
                         ('lineChart','LineChart'),
                         ('lineChart_Date','LineChart_Date'),
                         ('lineWithFocusChart','LineWithFocusChart'),
                         ('multiBarChart','MultiBarChart'),
                         ('stackedAreaChart','StackedAreaChart'),
                         ('multiBarHorizontalChart','MultiBarHorizontalChart'),
                         ('linePlusBarChart','LinePlusBarChart'),
                         ('cumulativeLineChart','CumulativeLineChart'),
                         ('discreteBarChart','DiscreteBarChart'),
                         ('discreteBarChart_Date','DiscreteBarChart_Date'),
                         ('scatterChart','ScatterChart'),
                         ('linePlusBarWithFocusChart','LinePlusBarWithFocusChart'),
                        )

    FILTER_TYPE_CHOICE = (('Date', 'Date'), 
                          ('Month', 'Month')
                         )

    datasource = models.ForeignKey(DataSource)
    name = models.CharField(max_length=50)
    chart_type = models.CharField(max_length=50, choices=CHART_TYPE_CHOICE)
    x_axis = models.ManyToManyField(DataFields, related_name='datafield_x_axis', null=True, blank=True)
    y_axis = models.ManyToManyField(DataFields, related_name='datafield_y_axis', null=True, blank=True)
    height = models.PositiveIntegerField(default=400, null=True, blank=True)
    width = models.PositiveIntegerField(default=600, null=True, blank=True)
    description = models.TextField(blank=True, null=True)
    is_chart = models.BooleanField(default=True)
    is_tabular = models.BooleanField(default=True)
    is_filter = models.BooleanField(default=False)
    filter_column = models.CharField(max_length=50, choices=FILTER_TYPE_CHOICE, null=True, blank=True)

    class Meta:
        db_table = "report_info"
        verbose_name = "report_info"

    def __unicode__(self):
        return self.name


class Dashboard(models.Model):
    name = models.CharField(max_length=50)
    report_info = models.ManyToManyField(ReportInfo,null=True, blank=True)

    class Meta:
        db_table = "dashboard"
        verbose_name = "dashboard"

    def __unicode__(self):
        return self.name
