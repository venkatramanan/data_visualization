from django.template.defaultfilters import register
from django.utils.safestring import mark_safe
from django.conf import settings

@register.simple_tag
def media_path(media_file):    
    return '%s%s' % (settings.MEDIA_URL,media_file)

@register.simple_tag
def virtual_path():    
    return settings.VIRTUAL_DIR