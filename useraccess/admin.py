from django.contrib import admin
from useraccess.models import UserDashboard, GroupReport

  
admin.site.register(UserDashboard)
admin.site.register(GroupReport)
