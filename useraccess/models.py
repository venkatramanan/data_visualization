from django.db import models
from django.contrib.auth.models import User, Group
from data_analytic.models import ReportInfo, Dashboard
# Create your models here.

class UserDashboard(models.Model):
    dashboard = models.ForeignKey(Dashboard)
    user = models.ForeignKey(User)
    
    class Meta:
        db_table = "userdashboard"
        verbose_name = "userdashboard"

    def __unicode__(self):
        return u'%s:%s' % (self.dashboard.name, self.user.username)    


class GroupReport(models.Model):
    report_info = models.ManyToManyField(ReportInfo,null=True, blank=True)
    group = models.ForeignKey(Group)
    
    class Meta:
        db_table = "groupreport"
        verbose_name = "groupreport"
        
    def __unicode__(self):
        return u'%s:%s' % (self.group, self.report_info)    